import plotly.graph_objects as go 

def plot_gps_points_with_osm(points):
    """
    Plots GPS points on a map with OpenStreetMap as the background.

    Args:
        points (list): A list of two GPSPoint objects to be plotted.
    """
    # Extract latitude, longitude, and text from points
    lats = [point.latitude for point in points]
    lons = [point.longitude for point in points]
    names = [point.name for point in points]

    # Create scattermapbox trace
    trace = go.Scattermapbox(
        lat=lats,
        lon=lons,
        mode="markers+text",
        marker=dict(size=10, color="blue"),
        text=names,
        textposition="bottom center"
    )

    # Create layout with OpenStreetMap as the background
    layout = go.Layout(
        title="GPS Points Plot with OpenStreetMap",
        mapbox=dict(
            style="open-street-map",  # Use OpenStreetMap as the tileset
            zoom=11,  # Set initial zoom level
            center=dict(lat=sum(lats) / len(lats), lon=sum(lons) / len(lons))  # Center the map around the points
        )
    )

    # Create figure
    fig = go.Figure(trace, layout)
    fig.update_layout(width=700,height=700)

    # Show plot
    fig.show()

